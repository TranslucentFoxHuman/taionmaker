﻿/*
 * mainwindow.cpp
 * Copyright (C) 2021 半透明狐人間
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "aboutdialog.h"

#include <stdio.h>
#include <string>
#include <cstdlib>

#include <QtCore/QDebug>


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_aboutAction_triggered() {
    AboutDialog *abtdiag = new AboutDialog(this);
    abtdiag->show();
}



void MainWindow::on_makeButton_clicked()
{
    int baseUpint = stoi(ui->baseUp->text().toStdString());
    int baseLowint = stoi(ui->baseLow->text().toStdString());
    int moveLevel = stoi(ui->moveLevelEdit->text().toStdString());

    int resLowint = 0;

    //qDebug() << "on_makeButton_clicked is started";

    if("高くする" == ui->howtoupCombo->currentText().toStdString()) {
            resLowint = baseLowint + (rand() % (moveLevel+1));
    } else if ("低くする" == ui->howtoupCombo->currentText().toStdString()) {
            resLowint = baseLowint - (rand() % (moveLevel+1));
    } else if ("おまかせ" == ui->howtoupCombo->currentText().toStdString()){
        int randomdata = rand() % 2;
        if (randomdata == 0) {
            resLowint = baseLowint + (rand() % (moveLevel+1));
        } else {
            resLowint = baseLowint - (rand() % (moveLevel+1));
        }
    }

    if (resLowint >=10) {
        resLowint -= 10;
    } else if (resLowint < 0){
        resLowint += 10;
    }
    std::string resultstr = std::to_string(baseUpint);
    resultstr = resultstr + "." + std::to_string(resLowint);
    ui->resultview->setText(QString::fromStdString(resultstr));

    if (ui->autoUpdateCheck->checkState() == Qt::Checked) {
        ui->baseLow->setText(QString::fromStdString(std::to_string(resLowint)));
    }
}
